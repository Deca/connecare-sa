package utils;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public class Utils {

    private static final String RDS = "rds";
    private static final String PMML = "pmml";
    private static final String CSV = "csv";

    public static boolean isModelSupported(String fileName){
        String extension = getExtension(fileName);
        return extension.equals(RDS) || extension.equals(PMML);
    }

    public static boolean isInputSupported(String fileName){
        String extension = getExtension(fileName);
        return extension.equals(CSV);
    }

    private static String getExtension(String fileName) {
        String ext = null;
        int i = fileName.lastIndexOf('.');

        if (i > 0 &&  i < fileName.length() - 1) {
            ext = fileName.substring(i+1).toLowerCase();
        }
        return ext;
    }
}
