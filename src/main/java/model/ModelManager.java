package model;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import model.interfaces.IModel;
import model.interfaces.IModelManager;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.*;
import org.jpmml.model.MetroJAXBUtil;
import org.jpmml.model.PMMLUtil;
import org.jpmml.rexp.Converter;
import org.jpmml.rexp.ConverterFactory;
import org.jpmml.rexp.RExp;
import org.jpmml.rexp.RExpParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public class ModelManager implements IModelManager {

    private String path;

    private double zeroThreshold = 1e-9;
    private double precision = 1e-9;

    private ModelManager(String path){
        super();
        this.path = path;
    }

    public static ModelManager newModelManager(String path){
        return new ModelManager(path);
    }

    @Override
    public String uploadModel(InputStream content, ModelType modelType, String description, String provider) {
        String uuid = UUID.randomUUID().toString();
        switch (modelType){
            case RDS:
                // Salvare il file, convertire in pmml e salvare anche il pmml
                this.createDirectoryForModel(uuid);
                File dir = new File(this.path + File.separator + uuid);
                dir.mkdir();
                File fileRDS = new File(this.path + File.separator + uuid + File.separator + "model.rds");
                this.convertModelToPmml(content,uuid + File.separator + "model.pmml");
                try {
                    OutputStream os = new FileOutputStream(fileRDS);
                    PrintWriter writer = new PrintWriter(this.path + File.separator + uuid + File.separator + "info.txt");
                    JSONObject obj = new JSONObject();
                    obj.put("id", uuid);
                    obj.put("provider", provider);
                    obj.accumulate("format", "RDS");
                    obj.accumulate("format", "PMML");
                    obj.put("description", description);
                    writer.println(obj);
                    writer.flush();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.updateModelsSummary(uuid, provider, description, new String[]{"RDS", "PMML"});
                return uuid;
            case PMML:
                // Salvare il file pmml
                this.createDirectoryForModel(uuid);
                File filePMML = new File(this.path + File.separator + uuid + File.separator + "model.pmml");
                try {
                    OutputStream os = new FileOutputStream(filePMML);
                    PrintWriter writer = new PrintWriter(this.path + File.separator + uuid + File.separator + "info.txt");
                    JSONObject obj = new JSONObject();
                    obj.put("id", uuid);
                    obj.put("provider", provider);
                    obj.put("format", new String[]{"PMML"});
                    obj.put("description", description);
                    writer.println(obj);
                    writer.flush();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.updateModelsSummary(uuid, provider, description, new String[]{"PMML"});
                return uuid;
            default:
                return null;
        }
    }

    private void createDirectoryForModel(String uuid){
        File dir2 = new File(this.path + File.separator + uuid);
        dir2.mkdir();
    }

    private void convertModelToPmml(InputStream content, String UUIDPath) {
        try {
            File file = new File(this.path + File.separator + UUIDPath);
            OutputStream os = new FileOutputStream(file);
            RExpParser parser = new RExpParser(content);

            System.out.println("Converting model to PMML...");
            RExp rexp = parser.parse();

            ConverterFactory converterFactory = ConverterFactory.newInstance();

            Converter<RExp> converter = converterFactory.newConverter(rexp);

            PMML pmml = converter.encodePMML();
            MetroJAXBUtil.marshalPMML(pmml, os);
            System.out.println("RDS model converted to PMML model");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void updateModelsSummary(String uuid, String provider, String description, String... formats){
        String content = null;
        try {
            BufferedReader in = new BufferedReader(new FileReader(this.path + File.separator + "summary.txt"));
            content = in.readLine();
        } catch (FileNotFoundException e) {
            try {
                PrintWriter writer = new PrintWriter(this.path + File.separator + "summary.txt");
                JSONObject obj = new JSONObject();
                obj.put("models", new JSONArray());
                content = obj.toString();
                writer.println(content);
                writer.flush();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = null;
        try {
            obj = new JSONObject(content);
            JSONObject o = new JSONObject();
            o.put("id", uuid);
            o.put("provider", provider);
            o.put("format", formats);
            o.put("description", description);
            obj.accumulate("models", o);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            PrintWriter writer = new PrintWriter(this.path + File.separator + "summary.txt");
            writer.println(obj);
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<IModel> getUploadedModels() {
        List<IModel> returnList = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(this.path + File.separator + "summary.txt"));
            String content = in.readLine();

            JSONObject obj = new JSONObject(content);
            JSONArray models = obj.getJSONArray("models");
            for(int i=0; i<models.length(); i++){
                JSONObject model = models.getJSONObject(i);
                String uuid = model.getString("id");
                String provider = model.getString("provider");
                String description = model.getString("description");
                JSONArray formats = model.getJSONArray("format");
                List<String> listFormats = new ArrayList<>();
                for(int j=0; j<formats.length(); j++){
                    listFormats.add(formats.getString(j));
                }
                IModel m = new Model(uuid, provider, listFormats, description);
                returnList.add(m);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return returnList;
    }

    @Override
    public String getModelDescription(String modelID) {
        String returnDescription = null;
        try {
            BufferedReader in = new BufferedReader(new FileReader(this.path + File.separator + "summary.txt"));
            String content = in.readLine();

            JSONObject obj = new JSONObject(content);
            JSONArray models = obj.getJSONArray("models");
            for(int i=0; i<models.length(); i++){
                JSONObject model = models.getJSONObject(i);
                String currentModelID = model.getString("id");
                if(modelID.equals(currentModelID)){
                    returnDescription = model.getString("description");
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnDescription;
    }

    @Override
    public InputStream downloadModel(String modelID, ModelType format) {
        String extension = format == ModelType.RDS ? "rds" : "pmml";
        File model = new File(this.path + File.separator + modelID + File.separator + "model." + extension);
        try {
            FileInputStream is = new FileInputStream(model);
            return is;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public InputStream downloadModel(String modelID) {
        File model = new File(this.path + File.separator + modelID);
        try {
            FileInputStream is = new FileInputStream(model);
            return is;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteModel(String modelID) {
        File modelDirectory = new File(this.path + File.separator + modelID);
        return modelDirectory.delete();
    }

    @Override
    public void evaluateData(String modelID, InputStream input, OutputStream output) {
        try {
            // Leggo il file di input e lo parso
            CsvUtil.Table inputTable = CsvUtil.readTable(input, null);
            List<? extends Map<FieldName, ?>> inputRecords = BatchUtil.parseRecords(inputTable, Parser.CSV_PARSER);

            // Prendo il modello PMML e faccio il marshalling
            File filePMML = new File(this.path + File.separator + modelID + File.separator + "model.pmml");
            InputStream is = new FileInputStream(filePMML);
            PMML pmml = PMMLUtil.unmarshal(is);

            // Credo l'evaluator attraverso la Factory
            ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();
            Evaluator evaluator = modelEvaluatorFactory.newModelEvaluator(pmml);

            // Verifico se c'è qualche errore
            evaluator.verify();

            List<InputField> inputFields = evaluator.getInputFields();
            List<InputField> groupFields = Collections.emptyList();

            if(evaluator instanceof HasGroupFields){
                HasGroupFields hasGroupfields = (HasGroupFields)evaluator;
                groupFields = hasGroupfields.getGroupFields();
            }

            if(inputRecords.size() > 0){
                Map<FieldName, ?> inputRecord = inputRecords.get(0);

                Sets.SetView<FieldName> missingGroupFields = Sets.difference(new LinkedHashSet<>(EvaluatorUtil.getNames(groupFields)), inputRecord.keySet());
                if(missingGroupFields.size() > 0){
                    throw new IllegalArgumentException("Missing group field(s): " + missingGroupFields.toString());
                }
            }

            if(evaluator instanceof HasGroupFields){
                HasGroupFields hasGroupFields = (HasGroupFields)evaluator;

                inputRecords = EvaluatorUtil.groupRows(hasGroupFields, inputRecords);
            }

            // Preparo la lista degli output
            List<Map<FieldName, ?>> outputRecords = new ArrayList<>(inputRecords.size());

            Map<FieldName, FieldValue> arguments = new LinkedHashMap<>();

            for(Map<FieldName, ?> inputRecord : inputRecords){
                arguments.clear();

                for(InputField inputField : inputFields){
                    FieldName name = inputField.getName();

                    FieldValue value = EvaluatorUtil.prepare(inputField, inputRecord.get(name));

                    arguments.put(name, value);
                }

                Map<FieldName, ?> result = evaluator.evaluate(arguments);

                outputRecords.add(result);
            }


            List<TargetField> targetFields = evaluator.getTargetFields();
            List<OutputField> outputFields = evaluator.getOutputFields();

            List<? extends ResultField> resultFields = Lists.newArrayList(Iterables.concat(targetFields, outputFields));

            CsvUtil.Table outputTable = new CsvUtil.Table();
            outputTable.setSeparator(inputTable.getSeparator());

            outputTable.addAll(BatchUtil.formatRecords(outputRecords, EvaluatorUtil.getNames(resultFields), Parser.CSV_FORMATTER));

            /*if((inputTable.size() == outputTable.size())){

                for(int i = 0; i < inputTable.size(); i++){
                    List<String> inputRow = inputTable.get(i);
                    List<String> outputRow = outputTable.get(i);

                    //outputRow.addAll(0, inputRow);
                }
            }*/

            CsvUtil.writeTable(outputTable, output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void compare(String modelID, InputStream input, InputStream expectedOutput) {
        try {
            File filePMML = new File(this.path + File.separator + modelID + File.separator + "model.pmml");
            InputStream is = new FileInputStream(filePMML);
            PMML pmml = PMMLUtil.unmarshal(is);

            CsvUtil.Table inputTable = CsvUtil.readTable(input, null);

            CsvUtil.Table outputTable = CsvUtil.readTable(expectedOutput, null);

            ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();

            Evaluator evaluator = modelEvaluatorFactory.newModelEvaluator(pmml);
            Predicate<FieldName> predicate = Predicates.alwaysTrue();

            List<? extends Map<FieldName, ?>> inputRecords = BatchUtil.parseRecords(inputTable, Parser.CSV_PARSER);
            for(Map<FieldName, ?> elem: inputRecords){
                System.out.println(elem);
            }

            List<? extends Map<FieldName, ?>> outputRecords = BatchUtil.parseRecords(outputTable, Parser.CSV_PARSER);

            List<Conflict> conflicts;

            Batch batch = createBatch(evaluator, inputRecords, outputRecords, predicate);
            conflicts = BatchUtil.evaluate(batch, new PMMLEquivalence(this.precision, this.zeroThreshold));

            for(Conflict conflict : conflicts){
                System.err.println(conflict);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static
    private Batch createBatch(final Evaluator evaluator, final List<? extends Map<FieldName, ?>> input, final List<? extends Map<FieldName, ?>> output, final Predicate<FieldName> predicate){
        Batch batch = new Batch(){

            @Override
            public Evaluator getEvaluator(){
                return evaluator;
            }

            @Override
            public List<? extends Map<FieldName, ?>> getInput(){
                return input;
            }

            @Override
            public List<? extends Map<FieldName, ?>> getOutput(){
                return output;
            }

            @Override
            public Predicate<FieldName> getPredicate(){
                return predicate;
            }

            @Override
            public void close(){
            }
        };

        return batch;
    }

}
