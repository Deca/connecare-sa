package model;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public enum ModelType {
    RDS, PMML;

    public static ModelType parseModelType(String modelType){
        switch (modelType){
            case "RDS":
                return RDS;
            case "PMML":
                return PMML;
            default:
                return null;
        }
    }

    public static ModelType getModelTypeFromFileExtension(String fileName){
        String f = fileName.toLowerCase();
        if(f.endsWith(".rds")){
            return RDS;
        } else if(f.endsWith("pmml")){
            return PMML;
        } else {
            return null;
        }
    }
}
