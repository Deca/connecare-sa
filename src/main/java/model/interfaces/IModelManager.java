package model.interfaces;

import model.ModelType;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public interface IModelManager {

    /**
     * Uploads model
     * @param content
     * @param modelType
     * @param provider
     * @return UUID of the model, null if there is an error
     */
    String uploadModel(InputStream content, ModelType modelType, String description, String provider);

    /**
     *
     * @return
     */
    List<IModel> getUploadedModels();

    /**
     *
     * @param modelID
     * @return
     */
    String getModelDescription(String modelID);

    /**
     *
     * @param modelID
     * @param format
     * @return
     */
    InputStream downloadModel(String modelID, ModelType format);

    /**
     *
     * @param modelID
     * @return
     */
    InputStream downloadModel(String modelID);

    /**
     *
     * @param modelID
     * @return
     */
    boolean deleteModel(String modelID);

    /**
     *
     * @param modelID
     * @param input
     */
    void evaluateData(String modelID, InputStream input, OutputStream output);

    /**
     *
     * @param input
     * @param expectedOutput
     */
    void compare(String modelID, InputStream input, InputStream expectedOutput);

}
