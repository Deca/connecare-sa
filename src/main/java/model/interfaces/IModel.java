package model.interfaces;

import model.ModelType;

import java.util.List;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 * Interface for a statistical model
 *
 */
public interface IModel {

    /**
     * Returns the identifier of a model
     * @return Model identifier
     */
    String getID();

    /**
     * Returns the identifier of the uploader
     * @return Uploader id
     */
    String getProvider();

    /**
     * Returns the type of the model - RDS, PMML...
     * @return Model type
     */
    List<ModelType> getModelType();

    /**
     * Returns the description of the model
     * @return Model description
     */
    String getDescription();

}
