package model;

import model.interfaces.IModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public class Model implements IModel{

    private String id;
    private String provider;
    private List<ModelType> modelTypes;
    private String description;

    public Model(String id, String provider, List<String> modelTypes, String description){
        super();
        this.id = id;
        this.provider = provider;
        this.modelTypes = this.parseModelTypes(modelTypes);
        this.description = description;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public String getProvider() {
        return this.provider;
    }

    @Override
    public List<ModelType> getModelType() {
        return this.modelTypes;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    private List<ModelType> parseModelTypes(List<String> list){
        List<ModelType> returnList = new ArrayList<>();
        for(String str: list){
            returnList.add(ModelType.parseModelType(str));
        }
        return returnList;
    }

    @Override
    public String toString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", this.id);
            obj.put("provider", this.provider);
            obj.put("description", this.description);
            for(ModelType type: this.modelTypes){
                obj.accumulate("format", type);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
