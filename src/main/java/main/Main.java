package main;

import view.MainPanel;

import javax.swing.*;
import java.awt.*;

// TODO Relazione, vedere se è possibile non modificare jpmml-R altrimenti modificare il progetto con le dipendenze
// TODO Cambiare path separator multipiattaforma
// TODO Dipendenze da Maven/Gradle
// TODO Goal - motivazioni, spiegazione di quello che ho fatto (processo), descrizione come è fatto il progetto, mini-tutorial, come usare la gui
/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public class Main {

    // Path to save models
    private static final String PATH = "C:\\Users\\Andrea\\Desktop\\Connecare\\Models";
    public static final String PROVIDER = "#12345";

    public static void main(String[] args){
        JFrame frame = new JFrame("Test Model Manager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1200, 600);

        frame.getContentPane().add(new MainPanel(PATH));
        frame.setVisible(true);
    }
}
