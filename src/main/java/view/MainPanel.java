package view;

import model.ModelManager;
import model.interfaces.IModelManager;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Andrea De Castri on 28/07/2017.
 *
 */
public class MainPanel extends JPanel {

    private IModelManager modelManager;

    private LoadingPanel loadingPanel;
    private ShowPanel showPanel;
    private EvaluatePanel evaluatePanel;

    public MainPanel(String path){
        super();
        this.modelManager = ModelManager.newModelManager(path);
        this.setLayout(null);

        this.loadingPanel = new LoadingPanel(modelManager);
        this.loadingPanel.setBounds(0,0, 250, 600);

        this.evaluatePanel = new EvaluatePanel(modelManager);
        this.evaluatePanel.setBounds(900, 0, 300, 600);

        this.showPanel = new ShowPanel(modelManager, evaluatePanel);
        this.showPanel.setBounds(272, 0 , 610, 600);

        JSeparator separator = new JSeparator(JSeparator.VERTICAL);
        separator.setBounds(250, 10, 2, 500);

        this.add(loadingPanel);
        this.add(separator);
        this.add(showPanel);
        this.add(evaluatePanel);
    }

}
