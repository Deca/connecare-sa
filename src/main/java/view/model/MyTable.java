package view.model;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;

public class MyTable extends JTable {

    private JScrollPane scrollPane;

	public MyTable(TableModel model, int rowHeight, int fromLeft, int fromTop, int x, int y, Color color, Integer... columns){
		super(model);
		this.setFillsViewportHeight(true);
		this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setRowHeight(rowHeight);
		this.setSizeColumns(columns);
		this.getTableHeader().setEnabled(false);
		this.getTableHeader().setBackground(color);
		this.scrollPane = new JScrollPane(this);
		this.scrollPane.setBounds(fromLeft, fromTop, x, y);
	}
	
	/**
	 * Sets the columns width.
	 * @param columns - it is a vector with the values of the columns width
	 */
	public void setSizeColumns(Integer... columns){
		int i = 0;
		int max = columns.length ;
		for(; i<max; i++){
			this.getColumnModel().getColumn(i).setPreferredWidth(columns[i]);
		}
	}
	
	/**
	 * Returns a JScrollPane.
	 * @return a JScrollPane where there is a JTable
	 */
	public JScrollPane returnScrollPane(){
		return this.scrollPane;
	}

}