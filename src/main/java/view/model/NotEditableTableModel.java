package view.model;

import javax.swing.table.DefaultTableModel;

public class NotEditableTableModel extends DefaultTableModel {

	public NotEditableTableModel(Object[][] data, String[] columnText){
		super(data,columnText);
	}

	@Override
	public boolean isCellEditable(int row, int col) {
        if (col < this.getColumnCount()) {
            return false;
        } else {
            return true;
        }
    }

}