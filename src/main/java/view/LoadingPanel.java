package view;

import main.Main;
import model.ModelType;
import model.interfaces.IModelManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by Andrea De Castri
 *
 */
public class LoadingPanel extends JPanel {

    private JButton buttonLoading;
    private JButton buttonUpload;
    private JFileChooser chooser;

    private ButtonGroup buttonGroup;
    private JRadioButton buttonRDS;
    private JRadioButton buttonPMML;

    private JTextArea textArea;

    private IModelManager modelManager;

    private InputStream currentModelInputStream;
    private ModelType currentModelType;

    private JLabel labelModelName;
    private JLabel labelSuccess;

    public LoadingPanel(IModelManager modelManager){
        super();
        this.modelManager = modelManager;
        this.setLayout(null);
        this.setBounds(0,0,300,600);

        this.buttonLoading = new JButton("Open Model");
        this.buttonLoading.setBounds(10, 10, 200, 30);

        this.labelModelName = new JLabel("No model opened");
        this.labelModelName.setBounds(10, 40, 200, 30);

        this.labelSuccess = new JLabel("");
        this.labelSuccess.setBounds(10, 350, 200, 30);

        this.chooser = new JFileChooser();
        this.buttonGroup = new ButtonGroup();

        this.buttonRDS = new JRadioButton("RDS");
        this.buttonPMML = new JRadioButton("PMML");

        this.buttonGroup.add(buttonRDS);
        this.buttonGroup.add(buttonPMML);

        this.buttonPMML.setBounds(20, 70, 120, 30);
        this.buttonRDS.setBounds(20, 100, 120, 30);
        this.buttonPMML.setEnabled(false);
        this.buttonRDS.setEnabled(false);

        this.textArea = new JTextArea("Write a description here!");
        this.textArea.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                textArea.setText("");
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        JScrollPane areaScrollPane = new JScrollPane(textArea);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        areaScrollPane.setBounds(10, 140, 200, 150);

        this.buttonUpload = new JButton("Upload Model");
        this.buttonUpload.setBounds(10, 310, 200, 30);

        this.chooser.setAcceptAllFileFilterUsed(false);

        this.buttonLoading.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadingPanel.this.openFileChooser();
            }
        });

        this.buttonUpload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(currentModelInputStream != null && currentModelType != null){
                    String uuid = LoadingPanel.this.modelManager.uploadModel(currentModelInputStream, currentModelType, textArea.getText(),
                            Main.PROVIDER);
                    if(uuid != null){
                        labelSuccess.setForeground(Color.GREEN);
                        labelSuccess.setText("Model uploaded");
                        labelSuccess.setBounds(10, 350, 200, 30);
                    }
                }
            }
        });

        this.add(buttonLoading);
        this.add(labelModelName);
        this.add(buttonPMML);
        this.add(buttonRDS);
        this.add(areaScrollPane);
        this.add(buttonUpload);
        this.add(labelSuccess);
    }

    private void openFileChooser(){
        int returnVal = this.chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = this.chooser.getSelectedFile();
            try {
                this.currentModelInputStream = new FileInputStream(file);
                ModelType type = ModelType.getModelTypeFromFileExtension(file.getName());
                if(type != null){
                    this.currentModelType = type;
                    this.setSelectionModelType(type);
                    this.labelModelName.setText("Model: " + file.getName());
                } else {
                    this.labelModelName.setText("Model not supported");
                }
            } catch (FileNotFoundException e) {
                this.currentModelInputStream = null;
                this.currentModelType = null;
                this.labelModelName.setText("File not found");
                e.printStackTrace();
            }
        }
    }

    private void setSelectionModelType(ModelType type){
        switch (type){
            case RDS:
                buttonRDS.setSelected(true);
                break;
            case PMML:
                buttonPMML.setSelected(true);
                break;
        }
    }

}
