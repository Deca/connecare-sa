package view;

import model.interfaces.IModelManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by Andrea on 12/09/2017.
 *
 */
public class EvaluatePanel extends JPanel {

    private IModelManager modelManager;

    private JButton buttonLoadInput;
    private JButton buttonEvaluate;

    private JButton buttonLoadExpectedOutput;
    private JButton buttonCompare;

    private JFileChooser chooser;

    private InputStream inputToEvaluate;
    private String modelForEvaluation;
    private InputStream inputExpectedOutput;

    public EvaluatePanel(IModelManager modelManager){
        super();
        this.modelManager = modelManager;
        this.setLayout(null);

        this.chooser = new JFileChooser();

        this.buttonLoadInput = new JButton("Load Input File");
        this.buttonLoadInput.setBounds(10, 10, 180, 30);

        this.buttonEvaluate = new JButton("Evaluate Input");
        this.buttonEvaluate.setBounds(10, 50, 180, 30);

        this.buttonLoadExpectedOutput = new JButton("Load Expected Output");
        this.buttonLoadExpectedOutput.setBounds(10, 160, 180, 30);

        this.buttonCompare = new JButton("Compare Output");
        this.buttonCompare.setBounds(10, 200, 180, 30);

        this.buttonEvaluate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(modelForEvaluation != null && inputToEvaluate != null){
                    EvaluatePanel.this.evaluate();
                } else {
                    System.out.println("Input or model not selected");
                }
            }
        });

        this.buttonLoadExpectedOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EvaluatePanel.this.openFileChooser(ChooserType.EXPECTED_OUTPUT);
            }
        });

        this.buttonCompare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(inputExpectedOutput != null){
                    EvaluatePanel.this.compare();
                } else {
                    System.out.println("Expected output not loaded");
                }
            }
        });

        this.buttonLoadInput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EvaluatePanel.this.openFileChooser(ChooserType.INPUT_FILE);
            }
        });

        this.add(buttonLoadInput);
        this.add(buttonEvaluate);
        this.add(buttonLoadExpectedOutput);
        this.add(buttonCompare);
    }

    public void setCurrentModel(String modelID){
        this.modelForEvaluation = modelID;
    }

    private void openFileChooser(ChooserType chooserType){
        int returnVal = this.chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = this.chooser.getSelectedFile();
            try {
                switch (chooserType){
                    case INPUT_FILE:
                        this.inputToEvaluate = new FileInputStream(file);
                        break;
                    case EXPECTED_OUTPUT:
                        this.inputExpectedOutput = new FileInputStream(file);
                        break;
                }
            } catch (FileNotFoundException e) {
                this.inputToEvaluate = null;
                e.printStackTrace();
            }
        }
    }

    private void evaluate(){
        try {
            File file = new File("src\\result.csv");
            FileOutputStream os = new FileOutputStream(file);
            this.modelManager.evaluateData(modelForEvaluation, inputToEvaluate, os);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void compare(){
        this.modelManager.compare(modelForEvaluation, inputToEvaluate, inputExpectedOutput);
    }

    private enum ChooserType {
        INPUT_FILE, EXPECTED_OUTPUT
    }

}
