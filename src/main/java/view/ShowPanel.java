package view;

import model.ModelType;
import model.interfaces.IModel;
import model.interfaces.IModelManager;
import view.model.MyTable;
import view.model.NotEditableTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.List;

/**
 * Created by Andrea on 09/09/2017.
 *
 */
public class ShowPanel extends JPanel {

    private IModelManager modelManager;

    private MyTable table;
    private NotEditableTableModel tableModel;

    private String[] columnNames = {"ID", "Provider", "Description", "Formats"};
    private Object[][] data;

    private JButton buttonRefresh;
    private JButton buttonDownload;

    private String currentModelID = null;

    private EvaluatePanel evaluatePanel;


    public ShowPanel(IModelManager modelManager, EvaluatePanel evaluatePanel){
        super();
        this.modelManager = modelManager;
        this.evaluatePanel = evaluatePanel;
        this.setLayout(null);

        this.tableModel = new NotEditableTableModel(data, columnNames);
        this.table = new MyTable(tableModel, 30, 10, 10, 600, 400, Color.YELLOW, new Integer[]{0, 80, 407, 110});
        this.table.removeColumn(table.getColumn("ID"));

        this.fillTable();

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        this.table.setDefaultRenderer(Object.class, centerRenderer);

        this.buttonRefresh = new JButton("Refresh List");
        this.buttonRefresh.setBounds(10, 420, 200, 30);

        this.buttonDownload = new JButton("Download Model");
        this.buttonDownload.setBounds(220, 420, 200, 30);
        this.buttonDownload.setEnabled(false);
        this.buttonDownload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    InputStream is = ShowPanel.this.modelManager.downloadModel(currentModelID, ModelType.PMML);
                    //InputStream is = ShowPanel.this.modelManager.downloadModel(currentModelID);
                    byte[] buffer = new byte[is.available()];
                    is.read(buffer);

                    File targetFile = new File("src\\" + currentModelID + ".pmml");
                    OutputStream outStream = new FileOutputStream(targetFile);
                    outStream.write(buffer);
                    is.close();
                    outStream.close();

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        this.table.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ShowPanel.this.showModelDetail(table.getSelectedRow());
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        this.buttonRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillTable();
            }
        });

        this.add(table.returnScrollPane());
        this.add(buttonRefresh);
        this.add(buttonDownload);
    }

    private void fillTable(){
        this.tableModel.setRowCount(0);
        List<IModel> list = this.modelManager.getUploadedModels();

        for(int i=0; i<list.size(); i++){
            IModel model = list.get(i);
            this.tableModel.addRow(new Object[]{model.getID(), model.getProvider(), model.getDescription(), model.getModelType().toString()});
        }
    }

    private void showModelDetail(int rowSelection){
        TableModel tableModel = table.getModel();
        this.currentModelID = (String) tableModel.getValueAt(rowSelection, 0);
        this.evaluatePanel.setCurrentModel(currentModelID);
        this.buttonDownload.setEnabled(true);
    }

}
